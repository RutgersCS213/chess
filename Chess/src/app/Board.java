package app;

import java.util.Scanner;

/**
 * @author Ragen Anthony This class initiates the board with its pieces and performs
 *         moves/checks if game is over etc.
 *
 */

public class Board {

	/**
	 * initialize pieces and values for black and white color.
	 */
	public Pieces[][] p = new Pieces[8][8];
	int wColor = 1;
	int bColor = 2;
	int[] wKPos = new int[2];
	int[] bKPos = new int[2];
	boolean draw = false;

	/**
	 * initialize the board with pieces on each side
	 *
	 */
	public Board() {

		// Set the board to null so that checking if there is something on the
		// board is easier.
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				p[i][j] = null;
			}
		}

		// set pawns for white side
		for (int i = 0; i < 8; i++) {
			p[6][i] = new pawn(wColor);
		}

		// set pawns for black side
		for (int i = 0; i < 8; i++) {
			p[1][i] = new pawn(bColor);
		}

		// set King positions
		p[0][4] = new King(bColor);
		p[7][4] = new King(wColor);
		wKPos[0] = 7;
		wKPos[1] = 4;
		bKPos[0] = 0;
		bKPos[1] = 4;

		// set Queen positions
		p[0][3] = new Queen(bColor);
		p[7][3] = new Queen(wColor);

		// set Rook positions
		p[0][0] = new Rook(bColor);
		p[0][7] = new Rook(bColor);
		p[7][0] = new Rook(wColor);
		p[7][7] = new Rook(wColor);

		// set Knight Positions
		p[0][1] = new Knight(bColor);
		p[0][6] = new Knight(bColor);
		p[7][1] = new Knight(wColor);
		p[7][6] = new Knight(wColor);

		// set Bishop Positions
		p[0][2] = new Bishop(bColor);
		p[0][5] = new Bishop(bColor);
		p[7][2] = new Bishop(wColor);
		p[7][5] = new Bishop(wColor);


		playGame(p);
	}


	/**
	 *
	 * Takes the player move (i.e. b4) and returns an arr to make it easier for
	 * 2D arr moves. example: a1 => int arr = 0, 7. a1 on chess board
	 * corresponds to 0, 7 on the 2D array.
	 *
	 * @param input
	 * @return output
	 *
	 */
	public int[] stringToArrInput(String input) {
		int[] output = new int[2];

		input = input.toLowerCase();

		String firstVal = input.substring(0, 1);
		String secondVal = input.substring(1, 2);

		if (firstVal.compareTo("a") == 0) {
			output[1] = 0;
		} else if (firstVal.compareTo("b") == 0) {
			output[1] = 1;
		} else if (firstVal.compareTo("c") == 0) {
			output[1] = 2;
		} else if (firstVal.compareTo("d") == 0) {
			output[1] = 3;
		} else if (firstVal.compareTo("e") == 0) {
			output[1] = 4;
		} else if (firstVal.compareTo("f") == 0) {
			output[1] = 5;
		} else if (firstVal.compareTo("g") == 0) {
			output[1] = 6;
		} else if (firstVal.compareTo("h") == 0) {
			output[1] = 7;
		} else { // INVALID MOVE: return -100
			output[1] = -100;
		}

		if (secondVal.compareTo("1") == 0) {
			output[0] = 7;
		} else if (secondVal.compareTo("2") == 0) {
			output[0] = 6;
		} else if (secondVal.compareTo("3") == 0) {
			output[0] = 5;
		} else if (secondVal.compareTo("4") == 0) {
			output[0] = 4;
		} else if (secondVal.compareTo("5") == 0) {
			output[0] = 3;
		} else if (secondVal.compareTo("6") == 0) {
			output[0] = 2;
		} else if (secondVal.compareTo("7") == 0) {
			output[0] = 1;
		} else if (secondVal.compareTo("8") == 0) {
			output[0] = 0;
		} else {
			output[0] = -100;
		}

		return output;

	}

	//Returns the pieces array needed to see where each item is on the board.
	public Pieces[][] getPieces(Pieces[][] p) {
		return p;
	}

	public void makeBoard(Pieces[][] p) {
		boolean whiteSpace = true;
		int numbers = 8;
		System.out.println("    a  b  c  d  e  f  g  h");
		for (int i = 0; i < 8; i++) {
			System.out.print(numbers +"  ");
			for (int j = 0; j < 8; j++) {
				if (j != 7) {
					if (p[i][j] == null) {
						if (whiteSpace) {
							System.out.print("   ");
							whiteSpace = false;
						} else {
							System.out.print("## ");
							whiteSpace = true;
						}
					} else {
						System.out.print(p[i][j].boardID() + " ");
						if (whiteSpace) {
							whiteSpace = false;
						} else {
							whiteSpace = true;
						}
					}
				} else {
					if (p[i][j] == null) {
						if (whiteSpace) {
							System.out.println("    "+numbers);
							numbers--;
						} else {
							System.out.println("## "+ " "+ numbers);
							numbers--;
						}
					} else {
						System.out.println(p[i][j].boardID() + "  "+numbers);
						numbers--;
					}
				}
			}
		}
		System.out.println("    a  b  c  d  e  f  g  h");

	}

	public boolean isInCheck (Pieces[][] p, int[] kingPos) {

		//If there is a pawn in the 2 diagonal spaces above the king, he is in check
		if(p[kingPos[0]][kingPos[1]].boardID().compareTo("bK") == 0) {

			//Check due to pawns
			if(p[kingPos[0]+1][kingPos[1]+1] != null && p[kingPos[0]+1][kingPos[1]+1].boardID().compareTo("wp") == 0 ||
					p[kingPos[0]+1][kingPos[1]-1] != null && p[kingPos[0]+1][kingPos[1]-1].boardID().compareTo("wp") == 0) {
				return true; 	//it is in check due to pawns.
			}

			//Check due to Knight
			if(p[kingPos[0]+2][kingPos[1]+1] != null && p[kingPos[0]+1][kingPos[1]+1].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]+2][kingPos[1]-1] != null && p[kingPos[0]+2][kingPos[1]-1].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]-2][kingPos[1]+1] != null && p[kingPos[0]-2][kingPos[1]-1].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]-2][kingPos[1]-1] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]+1][kingPos[1]+2] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]-1][kingPos[1]-2] != null && p[kingPos[0]-1][kingPos[1]-2].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]-1][kingPos[1]+2] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("wN") == 0 ||
					p[kingPos[0]+1][kingPos[1]-2] != null && p[kingPos[0]-1][kingPos[1]-2].boardID().compareTo("wN") == 0 ) {
					return true; //is in check due to knight.s
			}

			//Check due to Rook
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("wR") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("wR") == 0) {
					return true;
				} else {
					break;
				}
			}




			//Check due to bishops
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("wB") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("wB") == 0) {
					return true;
				} else {
					break;
				}
			}

			//Check due to Queens
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("wQ") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("wQ") == 0) {
					return true;
				} else {
					break;
				}
			}
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("wQ") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("wQ") == 0) {
					return true;
				} else {
					break;
				}
			}


		} else if (p[kingPos[0]][kingPos[1]].boardID().compareTo("wK") == 0) {
			//Check due to pawns
			if(p[kingPos[0]-1][kingPos[1]-1] != null && p[kingPos[0]-1][kingPos[1]-1].boardID().compareTo("bp") == 0 ||
					p[kingPos[0]-1][kingPos[1]+1] != null && p[kingPos[0]-1][kingPos[1]+1].boardID().compareTo("bp") == 0) {
				return true; 	//it is in check due to 2 pawns.
			}

			//Check due to Knight
			if(p[kingPos[0]+2][kingPos[1]+1] != null && p[kingPos[0]+1][kingPos[1]+1].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]+2][kingPos[1]-1] != null && p[kingPos[0]+2][kingPos[1]-1].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]-2][kingPos[1]+1] != null && p[kingPos[0]-2][kingPos[1]-1].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]-2][kingPos[1]-1] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]+1][kingPos[1]+2] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]-1][kingPos[1]-2] != null && p[kingPos[0]-1][kingPos[1]-2].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]-1][kingPos[1]+2] != null && p[kingPos[0]+1][kingPos[1]+2].boardID().compareTo("bN") == 0 ||
					p[kingPos[0]+1][kingPos[1]-2] != null && p[kingPos[0]-1][kingPos[1]-2].boardID().compareTo("bN") == 0 ) {
					return true; //is in check due to knight.s
			}

			//Check due to Rook
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("bR") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("bR") == 0) {
					return true;
				} else {
					break;
				}
			}


			//Check due to bishops
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("bB") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("bB") == 0) {
					return true;
				} else {
					break;
				}
			}
			//Check due to Queens
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("bQ") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][kingPos[1]] == null) {
					continue;
				}
				if(p[i][kingPos[1]] != null && p[i][kingPos[1]].boardID().compareTo("bQ") == 0) {
					return true;
				} else {
					break;
				}
			}
			for(int i = kingPos[0]-1;i>=0; i++) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("bQ") == 0) {
					return true;
				} else {
					break;
				}
			}

			for(int i = kingPos[0]+1;i<8; i--) {
				if(p[i][i] == null) {
					continue;
				}
				if(p[i][i] != null && p[i][i].boardID().compareTo("bQ") == 0) {
					return true;
				} else {
					break;
				}
			}

		}

		return false;
	}

	public void checkPromotion(Pieces[][] p, int[] finalPos) {
		// it is a white pawn/black pawn!
		if(p[finalPos[0]][finalPos[1]].boardID().compareTo("wp") == 0) {
			//enable promotion!
			if(finalPos[0] == 0) {
				System.out.println("Pawn reached end of the board! What do you want to promote it to? you can choose: wR, wN, wB, or wQ");

				Scanner sc = new Scanner(System.in);
				String input = sc.nextLine();

				if(input.length() != 2) {
					System.out.println("Invalid input. Try again.");
					while(input.length()!= 2 && (input.compareTo("wR") == 0 || input.compareTo("wN") == 0 ||
							input.compareTo("wB") == 0 || input.compareTo("wQ") == 0)) {
						sc = new Scanner(System.in);
						input = sc.nextLine();
					}
				}

				if (input.compareTo("wR") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Rook(wColor);
				} else if (input.compareTo("wN") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Knight(wColor);
				} else if (input.compareTo("wB") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Bishop(wColor);
				} else if (input.compareTo("wQ") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Queen(wColor);
				}

			}


		}else if (p[finalPos[0]][finalPos[1]].boardID().compareTo("bp") == 0) {

			//enable promotion
			if(finalPos[0] == 7) {
				System.out.println("Pawn reached end of the board! What do you want to promote it to? you can choose: bR, bN, bB, or bQ");

				Scanner sc = new Scanner(System.in);
				String input = sc.nextLine();

				if(input.length() != 2) {
					System.out.println("Invalid input. Try again.");
					while(input.length()!= 2 && (input.compareTo("bR") == 0 || input.compareTo("bN") == 0 ||
							input.compareTo("bB") == 0 || input.compareTo("bQ") == 0)) {
						System.out.println("What do you want to promote it to? you can choose: bR, bN, bB, or bQ");
						sc = new Scanner(System.in);
						input = sc.nextLine();
					}
				}

				if (input.compareTo("bR") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Rook(bColor);
				} else if (input.compareTo("bN") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Knight(bColor);
				} else if (input.compareTo("bB") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Bishop(bColor);
				} else if (input.compareTo("bQ") == 0) {
					p[finalPos[0]][finalPos[1]] = null;
					p[finalPos[0]][finalPos[1]] = new Queen(bColor);
				}

			}

		}
	}


	public void playGame(Pieces[][] p) {
		boolean gameEnd = false;
		int[] currPos, finalPos;
		boolean wTurn = true;		//used to alternate between the two turns

		while (gameEnd != true) {
			System.out.println("");
			if(draw == true) {

			}else {
				makeBoard(p);		//prints out the board
				System.out.println("You can resign by typing: 'resign' or offer a draw by typing 'draw?' and if the opponent player responds with 'draw'");
			}
			String currentPos, newPos;
			Scanner sc = new Scanner(System.in);	//take input


			if(wTurn) {
				System.out.print("Enter White's move (i.e. a3 a4): ");
			} else {
				System.out.println("");
				System.out.print("Enter Black's move (i.e. a3 a4): ");
			}
			String input = sc.nextLine();

			if(input.compareTo("resign") == 0) {
				if(wTurn) {
					System.out.println("Game resigned by White side.");
					System.out.println("Black side wins!");
					sc.close();
					return;
				} else {
					System.out.println("Game resigned by Black side.");
					System.out.println("White side wins!");
					sc.close();
					return;
				}

			}

			if(draw == true) {

				if(input.compareTo("draw") == 0) {
					System.out.println("Game ended in a draw. No one wins.");
					return;
				} else {
					System.out.println("draw failed. Continue playing.");
					draw = false;
					if(wTurn) {
						wTurn = false;
					} else {
						wTurn = true;
					}
				}
			}

			if (input.compareTo("draw?") == 0) {
				draw = true;
				System.out.println("Opponent player has offered a draw. Type 'draw' to end game with a draw. or continue playing by inputting your normal moves.");
				if(wTurn) {
					wTurn = false;
				} else {
					wTurn = true;
				}
				continue;
			}


			if(input.length() != 5) {
				System.out.println("INVALID INPUT FORMAT. Try something similar to: b2 b4");

				//Checks if the move is valid or not.
			} else {
				currentPos = input.substring(0, 2);
				currPos = stringToArrInput(currentPos);
				if(currPos[0] == -100 || currPos[1] == -100) {
					System.out.println("invalid move: "+ currentPos+ "  Try again.");
					continue;
				}
				newPos = input.substring(3,5);
				finalPos = stringToArrInput(newPos);
				if(finalPos[0] == -100 || finalPos[1] == -100) {
					System.out.println("invalid move: "+ newPos+ "Try again.");
					continue;
				}

				if(currPos[0] == finalPos[0] && currPos[1] == finalPos[1]) {
					System.out.println("Cant move piece to its own location! Try again.");
					continue;
				}

				if(p[currPos[0]][currPos[1]] == null){
					System.out.println("No piece exists at " + currentPos +" Try again.");
					continue;
				}



				//Executes white's turn
				if(wTurn) {
					if(p[currPos[0]][currPos[1]].isWhite() == true) {
						if(p[currPos[0]][currPos[1]].isValidMove(p, currPos, finalPos)){

							if(p[currPos[0]][currPos[1]].boardID().compareTo("wK") == 0) {
								wKPos[0] = finalPos[0];
								wKPos[1] = finalPos[1];
							}

							//CASTLING MOVE! This means the 2 spaces next to the king are empty.
							if(p[currPos[0]][currPos[1]].boardID().compareTo("wK") == 0) {
								if(currPos[0] == 7 && currPos[1] == 4 && finalPos[0] == 7 && finalPos[1] == 7) {
									p[7][5] = p[finalPos[0]][finalPos[1]];
									p[finalPos[0]][finalPos[1]] = null;
									p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								}
							}

							if(p[finalPos[0]][finalPos[1]] == null) {
								p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								p[currPos[0]][currPos[1]] = null;
								checkPromotion(p, finalPos);
							}else if(p[finalPos[0]][finalPos[1]] != null) {	//if there is a piece there, delete the piece and move the other piece there.
								if(p[finalPos[0]][finalPos[1]].boardID().compareTo("bK") == 0) {
									System.out.println("White side wins!");
									return;
								}
								p[finalPos[0]][finalPos[1]] = null;
								p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								p[currPos[0]][currPos[1]] = null;
								checkPromotion(p, finalPos);
							}

							//isInCheck(p, wKPos);
							wTurn = false;
						} else {
							System.out.println("INVALID MOVE. Try again.");
							continue;
						}
					}
				} else {
					if(p[currPos[0]][currPos[1]].isWhite() == false) {
						if(p[currPos[0]][currPos[1]].isValidMove(p, currPos, finalPos)) {

							if(p[currPos[0]][currPos[1]].boardID().compareTo("bK") == 0) {
								bKPos[0] = finalPos[0];
								bKPos[1] = finalPos[1];
							}

							//CASTLING FOR BLACK PIECE
							if(p[currPos[0]][currPos[1]].boardID().compareTo("bK") == 0) {
								if(currPos[0] == 0 && currPos[1] == 4 && finalPos[0] == 0 && finalPos[1] == 7) {
									p[0][5] = p[finalPos[0]][finalPos[1]];
									p[finalPos[0]][finalPos[1]] = null;
									p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								}
							}
							if(p[finalPos[0]][finalPos[1]] == null) {
								p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								p[currPos[0]][currPos[1]] = null;

								checkPromotion(p, finalPos);

							}else if(p[finalPos[0]][finalPos[1]] != null) {	//if there is a piece there, delete the piece and move the other piece there.
								if(p[finalPos[0]][finalPos[1]].boardID().compareTo("wK") == 0) {
									System.out.println("Black side wins!");
									return;
								}
								p[finalPos[0]][finalPos[1]] = null;
								p[finalPos[0]][finalPos[1]] = p[currPos[0]][currPos[1]];
								p[currPos[0]][currPos[1]] = null;
								checkPromotion(p, finalPos);
							}
							//isInCheck(p, bKPos);
							wTurn = true;
						} else {
							System.out.println("INVALID MOVE. Try again.");
							continue;
						}
					}
				}

			}



		}


	}

}
