package app;

public class Knight extends Pieces {
	
	int color;	//if color == 1, it is white. else, it is black.
	
	/**
	 * @param color
	 * make a new Knight piece with color 1 (white) or 2 (black).
	 */
	public Knight(int color) {
		this.color = color;
	}
	
	public String boardID() {
		if(this.isWhite()) {
			return "wN";
		} else {
			return "bN";
		}
	}
	
	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}
	
	
	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {
		
		//Either moving up 2 and to the right/left one or moving down 2 and to the right/left one
		if(Math.abs(currPos[0]-finalPos[0]) == 2) {
			if(Math.abs(currPos[1]-finalPos[1]) == 1) {	//return true if the move is up/down 2 and right/left one.
				return true;
			}else {
				return false;
			}
			
			// Moving left/right 2 spaces and up/down 1
		} else if (Math.abs(currPos[1]-finalPos[1]) == 2) {
			if(Math.abs(currPos[0]-finalPos[0]) == 1) {
				return true;
			}else {
				return false;
			}
		}
		
		return false;
		
	}
	
}
