package app;

public abstract class Pieces {

	public int pawn = 8;
	public int rook = 2;
	public int knight = 2;
	public int bishop = 2;
	public int queen = 1;
	public int king = 1;
	
	
	/**
	 * get the color associated with the player. if true, color = white
	 */
	public abstract boolean isWhite();
	
	/**
	 * Check if the move made is valid. Depends on Piece type.
	 * @return true or false if its valid or not.
	 */
	public abstract boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos);
	
	public abstract String boardID();
	
}
