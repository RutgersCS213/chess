package app;

public class Bishop extends Pieces {

	int color;	//if color == 1, it is white. else, it is black.

	/**
	 * @param color
	 * make a new Bishop piece with color 1 (white) or 2 (black).
	 */
	public Bishop(int color) {
		this.color = color;
	}

	public String boardID() {
		if(this.isWhite()) {
			return "wB";
		} else {
			return "bB";
		}
	}

	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}

	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {

		if ((Math.abs(currPos[0] - finalPos[0]) == Math.abs(currPos[1] - finalPos[1])) && currPos[0]!=finalPos[0]) { //checks if the piece moved diagonally
			if((currPos[0] - finalPos[0]) > 0){			//moving left
				if((currPos[1] - finalPos[1]) > 0){		//moving up

					int j = currPos[1];
					//iterate through array to check if there is a collision while moving diagonally up and left
					for (int i = currPos[0] - 1; i >= finalPos[0]; i--) {
						j--;
						//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
						//if the piece is the opposite color, return true. Else, return false.

						if (p[i][j] != null) {
							if(i == finalPos[0]) {
								if (p[finalPos[0]][finalPos[1]] != null
										&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
									return true;
								} else {
									return false;
								}

							} else {
								return false;
							}

						}

					}
					return true;
				}

				else{		//moving down

					int j = currPos[1];

					//iterate through array to check if there is a collision while moving diagonally down and left
					for (int i = currPos[0] - 1; i >= finalPos[0]; i--) {

						j++;
						//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
						//if the piece is the opposite color, return true. Else, return false.

						if (p[i][j] != null) {
							if(i == finalPos[0]) {
								if (p[finalPos[0]][finalPos[1]] != null
										&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
									return true;
								} else {
									return false;
								}

							} else {
								return false;
							}

						}

					}
					return true;

				}

			}
			else{		//moving right

				if((currPos[1] - finalPos[1]) > 0){		//moving up

					int j =currPos[1];
					//iterate through array to check if there is a collision while moving diagonally up and right
					for (int i = currPos[0] + 1; i <= finalPos[0]; i++) {

						j--;
						//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
						//if the piece is the opposite color, return true. Else, return false.

						if (p[i][j] != null) {
							if(i == finalPos[0]) {
								if (p[finalPos[0]][finalPos[1]] != null
										&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
									return true;
								} else {
									return false;
								}

							} else {
								return false;
							}

						}

					}
					return true;
				}

				else{	//moving down

					int j = currPos[1];
					//iterate through array to check if there is a collision while moving diagonally down and right
					for (int i = currPos[0] + 1; i <= finalPos[0]; i++) {
						j++;
						//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
						//if the piece is the opposite color, return true. Else, return false.

						if (p[i][j] != null) {
							if(i == finalPos[0]) {
								if (p[finalPos[0]][finalPos[1]] != null
										&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
									return true;
								} else {
									return false;
								}

							} else {
								return false;
							}

						}

					}
					return true;

				}

			}
		}
		else{
			return false;	//piece did not move diagonally
		}

	}

}
