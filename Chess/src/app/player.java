package app;

//implements player with pieces associated with each player.
public class player extends Pieces {
	int playerNum;
	player next;
	
	//set the player # (i.e. player1 or player2)
	public player(int num) {
		this.playerNum = num;
	}
	
	//default constructor sets a new player to 1
	public player() {
		this.playerNum = 1;
	}
	
	//find out which player is playing
	public int getPlayerNum() {
		return this.playerNum;
	}
	
	
	public boolean isWhite() {
		if (this.playerNum == 1){
			return true;
		}else {
			return false;
		}
	}
	
	//UNneeded method... dont know how to get rid of it without messing everythign up.
	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {
		return false;
	}
	public String boardID() {
		return "dne";
	}
	
	
}
