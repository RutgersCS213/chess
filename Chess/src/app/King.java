package app;

public class King extends Pieces {

	int color;	//if color == 1, it is white. else, it is black.
	
	
	/**
	 * @param color
	 * make a new King piece with color 1 (white) or 2 (black).
	 */
	public King(int color) {
		this.color = color;
	}
	
	public String boardID() {
		if(this.isWhite()) {
			return "wK";
		} else {
			return "bK";
		}
	}
	
	
	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}
	
	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) { 
		
		//CASTLING for white piece
		if(this.isWhite()) {
			if(currPos[0] == 7 && currPos[1] == 4) {
				if (finalPos[0] == 7 && finalPos[1] == 7) {		//CASTLING MOVE
					if(p[currPos[0]][currPos[1]+1] == null && p[currPos[0]][currPos[1]+2] == null) {
						return true;
					} else {
						return false;
					}
				}
			}
		} else {	//CASTLING for black piece
			if(currPos[0] == 0 && currPos[1] == 4) {
				if (finalPos[0] == 0 && finalPos[1] == 7) {		//CASTLING MOVE
					if(p[currPos[0]][currPos[1]+1] == null && p[currPos[0]][currPos[1]+2] == null) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
		
		//moving and capturing your own piece!!
		if(p[finalPos[0]][finalPos[1]] != null && p[finalPos[0]][finalPos[1]].isWhite() == p[currPos[0]][currPos[1]].isWhite()) {
			return false;
		}
		
		//Moving regularly anywhere
		//Only checks if you can move up down left right etc. 1 move anywhere.
		if(Math.abs((currPos[0] - finalPos[0])) == 1) {		//if the move is within 1 row.
			if(Math.abs(currPos[1] - finalPos[1]) == 1) {	//move is also within 1 column Moving DIAGONALLY
				return true;
			} else if (currPos[1] - finalPos[1] == 0) {		//moving vertically ONLY
				return true;
			}
		} else if (currPos[0] - finalPos[0] == 0) {		// moving horizontally ONLY
			if(Math.abs(currPos[1] - finalPos[1]) == 1) {	//move is also within 1 column
				return true;
			}
		} else if (Math.abs((currPos[0] - finalPos[0])) > 1 || Math.abs((currPos[1] - finalPos[1])) > 1) {
			return false;
		}
			
		return false;
	}
	
	
}
