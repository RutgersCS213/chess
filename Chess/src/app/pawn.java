package app;

//rules
public class pawn extends Pieces {

	int color;	//if color == 1, it is white. else, it is black.

	//make a new pawn piece with color 1 or 2.
	public pawn(int color) {
		this.color = color;
	}

	public String boardID() {
		if(this.isWhite()) {
			return "wp";
		} else {
			return "bp";
		}

	}


	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}

	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {

		//Check valid move for white piece
		if(this.isWhite()) {
			//Initial state. Can move 2 steps forward.
			if(currPos[0] == 6) {
				if(finalPos[0] == currPos[0]-2 && finalPos[1] == currPos[1]){	//Move forward 2 in the same column
					if(p[currPos[0]-1][currPos[1]] == null && p[currPos[0]-2][currPos[1]] == null) {
						//Checked that there is nothing blocking the pawns way.
						return true;
					} else {
						return false; 	//There is something blocking the pawns way. INVALID MOVE.
					}
				}else if (finalPos[0] == currPos[0]-1 && finalPos[1] == currPos[1]) {	//Move forward one spot. Checks if there is stuff blocking way.
					if(p[currPos[0]-1][currPos[1]] == null) {
						return true;
					}else {
						return false;
					}
				}

			} else if (finalPos[0] == currPos[0]-1 && finalPos[1] == currPos[1]) {	//Move forward one spot. Checks if there is stuff blocking way.
				if(p[currPos[0]-1][currPos[1]] == null) {
					return true;
				}else {
					return false;
				}

			// if moving forward and moving left or right 1, then
			} else if (finalPos[0] == currPos[0]-1 && ((finalPos[1] == currPos[1]-1) || finalPos[1] == currPos[1]+1)) {
				if(p[finalPos[0]][finalPos[1]] != null) {		//In the process of capturing a piece
					return true;
				} else {
					if(finalPos[0]==2 && p[finalPos[0]+1][finalPos[1]].getClass()== pawn.class){	//Checking for ENPASSANT
						p[finalPos[0]+1][finalPos[1]] = null;	//removes pawn
						return true;
					}
					else	return false;
				}
			}
		}

		//Check move for black pieces
		if(this.isWhite() == false) {
			//Initial state. Can move 2 steps forward.
			if(currPos[0] == 1) {
				if(finalPos[0] == currPos[0]+2 && finalPos[1] == currPos[1]){	//Move forward 2 in the same column
					if(p[currPos[0]+1][currPos[1]] == null && p[currPos[0]+2][currPos[1]] == null) {
						//Checked that there is nothing blocking the pawns way.
						return true;
					} else {
						return false; 	//There is something blocking the pawns way. INVALID MOVE.
					}
				}else if (finalPos[0] == currPos[0]+1 && finalPos[1] == currPos[1]) {	//Move forward one spot. Checks if there is stuff blocking way.
					if(p[currPos[0]+1][currPos[1]] == null) {
						return true;
					}else {
						return false;
					}
				}
			} else if (finalPos[0] == currPos[0]+1 && finalPos[1] == currPos[1]) {	//Move forward one spot. Checks if there is stuff blocking way.
				if(p[currPos[0]+1][currPos[1]] == null) {
					return true;
				}else {
					return false;
				}

			// if moving forward and moving left or right 1, then
			} else if (finalPos[0] == currPos[0]+1 && ((finalPos[1] == currPos[1]-1) || finalPos[1] == currPos[1]+1)) {
				if(p[finalPos[0]][finalPos[1]] != null) {		//In the process of capturing a piece
					return true;
				} else {
					if(finalPos[0]==5 && p[finalPos[0]-1][finalPos[1]].getClass()== pawn.class){ //checking for ENPASSANT
						p[finalPos[0]-1][finalPos[1]] = null;	//removes pawn
						return true;
					}
					else	return false;
				}
			}
		}


		return false;
	}




}
