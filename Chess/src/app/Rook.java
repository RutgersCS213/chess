package app;

public class Rook extends Pieces {

	int color;	//if color == 1, it is white. else, it is black.

	/**
	 * @param color
	 * make a new Rook piece with color 1 (white) or 2 (black).
	 */
	public Rook(int color) {
		this.color = color;
	}

	public String boardID() {
		if(this.isWhite()) {
			return "wR";
		} else {
			return "bR";
		}
	}

	//If color == white, return true. Else, return false.
	public boolean isWhite() {
		if (this.color == 1) {
			return true;
		}
		return false;
	}

	public boolean isValidMove(Pieces[][] p, int[] currPos, int[] finalPos) {

		// moving up towards black side of the board VERTICAL MOVE UP
		if (currPos[0] > finalPos[0] && currPos[1] == finalPos[1]) {

			//iterate through array to check if there is a collision
			for (int i = currPos[0] - 1; i >= finalPos[0]; i--) {

				//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
				//if the piece is the opposite color, return true. Else, return false.
				if (p[i][currPos[1]] != null) {
					if(i == finalPos[0]) {
						if (p[finalPos[0]][finalPos[1]] != null
								&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}

				}

			}
			return true;

			//HORIZONTAL MOVE TO THE LEFT
		} else if (currPos[1] > finalPos[1] && currPos[0] == finalPos[0]) {

			//iterate through array to check if there is a collision
			for (int i = currPos[1] - 1; i >= finalPos[1]; i--) {

				//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
				//if the piece is the opposite color, return true. Else, return false.
				if (p[currPos[0]][i] != null) {
					if(i == finalPos[1]) {
						if (p[finalPos[0]][finalPos[1]] != null
								&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}

				}

			}
			return true;

			//VERTICAL MOVE DOWN
		} else if (currPos[0] < finalPos[0] && currPos[1] == finalPos[1]) {

			//iterate through array to check if there is a collision
			for (int i = currPos[0] + 1; i <= finalPos[0]; i++) {

				//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
				//if the piece is the opposite color, return true. Else, return false.
				if (p[i][currPos[1]] != null) {
					if(i == finalPos[0]) {
						if (p[finalPos[0]][finalPos[1]] != null
								&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}

				}

			}
			return true;
			//HORIZONTAL MOVE RIGHT
		} else if (currPos[1] < finalPos[1] && currPos[0] == finalPos[0]) {

			//iterate through array to check if there is a collision
			for (int i = currPos[1] + 1; i <= finalPos[1]; i++) {

				//If there is a collision before the final value then it is invalid move. If it is the final value, check if the piece is the opposite color.
				//if the piece is the opposite color, return true. Else, return false.
				if (p[currPos[0]][i] != null) {
					if(i == finalPos[1]) {
						if (p[finalPos[0]][finalPos[1]] != null
								&& p[finalPos[0]][finalPos[1]].isWhite() != this.isWhite()) {
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}
				}

			}
			return true;

		}
		//INVALID MOVE
		return false;
	}
}
